= Te lo traigo de mi Pueblo: Plan de trabajo PEC1
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2016-10-03
:revnumber: {project-version}
:example-caption!:
ifndef::imagesdir[:imagesdir: images]
ifndef::sourcedir[:sourcedir: ../java]

En este documento se describe el plan de trabajo que se va a seguir para
el desarrollo del producto final. Para ello introduciremos brevemente el objetivo
del mismo, y siguiendo una metodología ágil definiremos unas épicas que
ayuden a visionar el ámbito. Asi mismo, de cada una de ellas definiremos unas primeras
historias de usuario que sirvan para inicializar el Product Backlog sobre el que trabajaremos.

Se describe la arquitectura propuesta así como la tencología y herramientas que
se utilizarán junto con un calendario orientativo sobre los Sprint a realizar
para la consecución del producto.

== Incepción

include::fragment/plan_de_trabajo/incepcion.asciidoc[]

== Objetivo

include::fragment/plan_de_trabajo/objetivo.asciidoc[]

== Épicas

include::fragment/plan_de_trabajo/epicas.asciidoc[]

== Tecnología y Arquitectura

include::fragment/plan_de_trabajo/tecnologia.asciidoc[]

== Artefactos

include::fragment/plan_de_trabajo/artefactos.asciidoc[]

== Herramientas de desarrollo y despliegue

include::fragment/plan_de_trabajo/herramientas.asciidoc[]

== Riesgos

include::fragment/plan_de_trabajo/riesgos.asciidoc[]

== Planificación

include::fragment/plan_de_trabajo/planificacion.asciidoc[]

[glossary]
== Glosario de términos

include::fragment/plan_de_trabajo/glosario.asciidoc[]

