[[codificacion_grails]]Para el backend hemos usado Grails ( http://www.grails.org ) como framework de desarrollo. Grails nos proporciona un
entorno donde la convención prevalece sobre la configuración de tal forma que si seguimos las convenciones establecidas
(nombre, rutas, etc) prácticamente no tendremos que realizar ninguna tarea de configuración.

Así mismo Grails proporciona todo un stack J2EE ( spring-framework, hibernate, sitemesh, etc ) junto con un ecosistema
de plugins muy amplio destinado a proporcionar un desarrollo muy rápido.

Por último comentar que Grails se basa en el lenguaje Groovy, el cual es un lenguaje dinámico que corre sobre una JVM
y que cuenta con una gran popularidad actualmente.

Respecto de nuestra aplicación, a continuación se describen los artefactos de interés:

* grails-app

** controllers

*** telotraigodemipueblo.UrlMappgings: configuración de puntos de entrada a los diferentes controllers

*** telotraigodemipueblo/api/core: Controllers de uso básico (registrarse, mi perfil, invitar a unirse, etc)

*** telotraigodemipueblo/api/crud: Controllers orientados al CRUD de diferentes recursos (lugares, productos,etc) relacionados
con un usuario logeado

*** telotraigodemipueblo/api/network: Controllers destinados a interconectar a un usuario con su red (hacer un pedido,
ver viajes en mi red, etc)

** domain

***  ttp: Objetos de dominio con etiquetas necesarias para la persistencia. Siguiendo las convenciones de Grails las
propiedades de estos serán persistidas en el motor de datos.

** views: Siguiendo las conveciones de Grails cada carpeta está destinada a renderizar los diferentes objetos en formato json

** services

*** telotraigodemipueblo/EventToMessageService: Servicio destinado a recibir en un solo punto los eventos que genera la
aplicación (como por ejemplo LugarCreado, PedidoSolicitado, etc) y convertirlos en Mensajes hacia los usuarios
interesados.

* src

** integration-test/groovy/ttp

*** domain: Test de integración (Spock Framework) destinados a validar las querys básicas de los objetos de dominio.

*** docuapi: Test de integración (Spock Framework) destinados a validar las diferentes APIs. Estos test son usados así
   mismo para generar la documentación del API creando cada uno de ellos fragmentos de documentación relativos a la
   petición REST (parámetros de envío, cabeceras, respuesta esperada, etc). En caso de que un test falle o no la
   respuesta recibida no coincida con la espera se aborta la generación del aplicativo. Debido a que estos test se ejecutan
   a través de HTTP contra el propio servidor se han desarrollado una serie de utilidades que nos permitan realizar
   la autentificación, crear elementos para pruebas, etc.


* plugins: en el fichero *build.gradle* definimos los plugins necesarios para nuestra aplicación. En concreto para
 TelotraigodemiPueblo hemos añadido "org.grails.plugins:spring-security-rest:2.0.0.M2" (para securizar las llamadas
 al API), 'org.springframework.data:spring-data-neo4j-rest:3.4.6.RELEASE' (para conectar con una base de datos Neo4j
 remota), y  "org.springframework.restdocs:spring-restdocs-core:$springRestdocsVersion" (para generar la documentación
 del API)

