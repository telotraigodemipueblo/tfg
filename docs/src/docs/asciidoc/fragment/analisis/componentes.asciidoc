
En esta fase realizamos una definición de componentes de alto nivel donde distinguimos
un cliente que comunicará con diferentes partes del API, las cuales a su vez delegarán
en los servicios oportunos la resolución de la petición. Todos los servicios utilizarán
el mismo repositorio (Neo4j).

[plantuml,diagrama,png]
....
skinparam packageStyle rect
title Componentes

node "Cliente"{
    [SinglePage Component]
}

frame "Api"{
    Login - [SecurityCtrl]
    [SinglePage Component] -> Login

    Register - [RegisterCtrl]
    [SinglePage Component] -> Register

    Search - [SearchCtrl]
    [SinglePage Component] -> Search

    MyNetwork - [MyNetworkCtrl]
    [SinglePage Component] -> MyNetwork
}

frame "Services"{
    [SecuritySrv]
    [CommunicationSrv]
    [MyNetworkSrv]
}

database "Neo4j" {
  folder "TTP" {
    [Database]
  }
}


[SecurityCtrl] --> [SecuritySrv]
[RegisterCtrl] --> [SecuritySrv]
[SearchCtrl] --> [MyNetworkSrv]
[MyNetworkCtrl] --> [MyNetworkSrv]

[SecuritySrv] --> [Database]
[MyNetworkSrv] --> [Database]
[CommunicationSrv] --> [Database]

....
