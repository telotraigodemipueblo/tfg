
[plantuml,casos-de-uso-network,png]
....
left to right direction
skinparam packageStyle rect
title Mi red

actor Usuario as usuario
actor Sistema as sistema

rectangle red{
    usuario -- (envio solicitud amistad)

    (envio mensaje) .> (envio solicitud amistad) : extend

    usuario -- (solicitud amistad aceptada)
    (solicitud amistad aceptada) <. (envio mensaje): extend

    usuario -- (solicitud amistad ignorada)

    usuario -- (ver mensajes)

    (envio mensaje) -- sistema
    (ver mensajes) -- sistema
}

....
