# Te lo Traigo de mi Pueblo

Trabajo Fin de Grado de Jorge Aguilera González 2016-17 por la UOC

## Documentación

Puede acceder a la documentación de este proyecto a través del siguiente enlace
 https://telotraigodemipueblo.gitlab.io/tfg
 
## Test

Puede acceder al ultimo informe de test a través del siguiente enlace
 https://telotraigodemipueblo.gitlab.io/reports
 
## API

Puede acceder a la documentación del API a través del siguiente enlace
 https://telotraigodemipueblo.gitlab.io/api
  