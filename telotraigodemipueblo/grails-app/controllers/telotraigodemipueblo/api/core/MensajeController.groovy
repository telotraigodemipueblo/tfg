package telotraigodemipueblo.api.core

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.rest.*
import grails.converters.*
import org.grails.web.json.JSONObject
import ttp.Mensaje
import ttp.TipoMensaje
import ttp.Usuario

@Secured('ROLE_USER')
class MensajeController extends RestfulController<Mensaje> {

    static responseFormats = ['json']


    MensajeController() {
        super(Mensaje)
    }

    SpringSecurityService springSecurityService

    protected List<Mensaje> listAllResources(Map params) {
        Usuario user = springSecurityService.currentUser as Usuario
        String recents = ''
        if(params.onlynews)
            recents = 'where m.leido is null'
        List<Mensaje> ret = Mensaje.cypherStatic("""MATCH (m:Mensaje)-[:DESTINATARIOS]->
            (u:Usuario {username:{1}}) ${recents}
            return m order by m.enviado desc limit $params.max""",
            [user.username]).toList(Mensaje)
        ret
    }

    protected getObjectToBind() {
        def json = request.JSON
        def ret =[
                tipo : json.tipo ?: TipoMensaje.MENSAJE,
                texto: json.texto ?: '',
                remitente: springSecurityService.currentUser as Usuario,
                destinatarios:[],
                enviado : new Date(),
                leido : json.leido
        ]
        json.destinatarios.each{ d->
            Usuario destinatario = Usuario.findById( d.id )
            assert destinatario != null
            ret.destinatarios.add destinatario
        }
        ret
    }

}
