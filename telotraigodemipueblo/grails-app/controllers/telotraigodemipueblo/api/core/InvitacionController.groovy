package telotraigodemipueblo.api.core

import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.rest.*
import grails.converters.*
import grails.transaction.Transactional
import telotraigodemipueblo.TipoEvento
import ttp.Mensaje
import ttp.TipoMensaje
import ttp.Usuario

import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.OK

/**
 * Created by jorge on 13/11/16.
 */
@Secured('ROLE_USER')
class InvitacionController {

    static responseFormats = ['json']

    static allowedMethods = [send: "POST", accept: "POST"]

    SpringSecurityService springSecurityService

    @Transactional
    def send(){
        Usuario remitente = springSecurityService.currentUser as Usuario
        def json = request.JSON
        assert json.destinatarios
        assert json.destinatarios.size()
        List<Usuario>destinatarios = []
        json.destinatarios.each{ d->
            Usuario destinatario = Usuario.get(d.id)
            assert destinatario
            destinatarios.add(destinatario)
        }

        Mensaje instance = new Mensaje(
                tipo:TipoMensaje.INVITACION,
                texto: "Hola, me gustaría que me admitieras en tu red",
                enviado: new Date(),
                leido:null,
                remitente:remitente
        )
        destinatarios.each{
            instance.addToDestinatarios(it)
        }

        instance.validate()
        if (instance.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond instance.errors, view:'create' // STATUS CODE 422
            return
        }
        instance.save flush: true
        respond instance, [status: CREATED]
    }

    @Transactional
    def accept(){
        def json = request.JSON
        Mensaje instance = Mensaje.get(json.mensaje)
        assert instance

        Usuario me = springSecurityService.currentUser as Usuario
        Usuario remitente = instance.remitente

        remitente.amigos(me)
        remitente.save flush: true
        me.save flush: true

        instance.leido = new Date()
        instance.save flush: true

        notify TipoEvento.NUEVO_AMIGO, [
                usuario: me.username,
                destinatario: remitente.username
        ]


        respond instance, [status: OK]
    }

}
