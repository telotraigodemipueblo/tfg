package telotraigodemipueblo.api.network

/**
 * Created by jorge on 8/12/16.
 */
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.rest.*
import grails.converters.*
import org.grails.web.json.JSONObject
import ttp.Lugar
import ttp.Producto
import ttp.Usuario

@Secured('ROLE_USER')
class ProductoController extends RestfulController<Producto>{

    ProductoController(){
        super(Producto,true)
    }

    SpringSecurityService springSecurityService

    protected List<Producto> listAllResources(Map params) {
        params.max = params.max ?: 10
        Lugar l = Lugar.get(params.lugarId)
        List<Producto> ret  =[]
        l?.tiene.each{ ret.add it }
        ret ?: []
    }

}