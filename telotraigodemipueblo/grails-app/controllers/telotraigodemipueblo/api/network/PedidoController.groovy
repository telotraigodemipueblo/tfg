package telotraigodemipueblo.api.network

/**
 * Created by jorge on 9/12/16.
 */
import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.rest.*
import grails.converters.*
import org.grails.web.json.JSONObject
import telotraigodemipueblo.TipoEvento
import ttp.Lugar
import ttp.Pedido
import ttp.Producto
import ttp.Usuario
import ttp.Viaje

@Secured('ROLE_USER')
class PedidoController extends RestfulController<Pedido>{

    PedidoController(){
        super(Pedido)
    }

    SpringSecurityService springSecurityService

    protected List<Pedido> listAllResources(Map params) {
        params.max = params.max ?: 10
        Usuario user = springSecurityService.currentUser as Usuario
        Viaje viaje = Viaje.get(params.viajeId)
        assert  viaje

        List<Pedido> ret =  []
        ret.addAll viaje.pedidos.findAll{ it.solicitante == user }
        ret
    }

    protected getObjectToBind() {
        def instance = request.JSON
        instance.producto = Producto.get(instance.producto)
        instance.solicitante = springSecurityService.currentUser as Usuario
        instance
    }

    protected Pedido saveResource(Pedido resource) {
        Viaje viaje = Viaje.get(params.viajeId)
        viaje.addToPedidos resource
        viaje.save flush: true

        Usuario user = springSecurityService.currentUser as Usuario
        notify TipoEvento.NUEVO_PEDIDO, [
                usuario: user.username,
                viaje: viaje.id,
                destinatario: viaje.lugar.usuario.username,
                producto: resource.producto.texto,
                cantidad: resource.cantidad
        ]

        resource
    }
}
