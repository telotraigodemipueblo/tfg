package telotraigodemipueblo.api.crud


import grails.plugin.springsecurity.SpringSecurityService
import grails.plugin.springsecurity.annotation.Secured
import grails.rest.*
import grails.converters.*
import ttp.Lugar
import ttp.Producto

@Secured('ROLE_USER')
class ProductoRestController extends RestfulController<Producto> {
    static responseFormats = ['json', 'xml']

    ProductoRestController(){
        super(Producto,false)
    }

    protected getObjectToBind() {
        def instance = request.JSON
        instance.lugar = Lugar.get(params.conoceId)
        instance
    }

    protected List<Producto> listAllResources(Map params) {
        params.max = params.max ?: 10
        Lugar l = Lugar.get(params.conoceId)
        List<Producto> ret  =[]
        l?.tiene.each{ ret.add it }
        ret ?: []
    }

    protected Producto saveResource(Producto resource) {
        Lugar lugar = Lugar.get(params.conoceId)
        lugar.addToTiene resource
        resource.lugar = lugar
        lugar.save flush: true
        resource
    }

}
