<!doctype html>
<html lang="en" class="no-js">
<head>
    <base href="/${request.contextPath}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>Welcome to Telotraigodemipueblo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <style type="text/css">
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
    </style>

    <asset:stylesheet src="application.css"/>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />

    <script type="text/javascript">
        window.contextPath = "${request.contextPath}";
    </script>
</head>

<body ng-app="telotraigodemipueblo" ng-controller="IndexController as vm">

    <div class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" ng-click="navExpanded = !navExpanded">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/index/welcome">
                    <i class="fa grails-icon">
                        <asset:image src="grails_logo.png"/>
                    </i> <span>Inicio</span>
                </a>
            </div>
            <div class="navbar-collapse collapse" aria-expanded="false" style="height: 0.8px;" uib-collapse="!navExpanded">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown" uib-dropdown  ng-show="authenticated">
                        <a href="#" class="dropdown-toggle" uib-dropdown-toggle role="button" aria-haspopup="true" aria-expanded="false">Menu<span class="caret"></span></a>
                        <ul class="dropdown-menu" uib-dropdown-menu>
                            <li><a href="/index/mislugares">Mis Pueblos</a></li>
                            <li><a href="/index/networkviajes">Viajes en mi red</a></li>
                            <li><a href="#" ng-click="vm.salir()">Cerrar sesion</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div id="content" role="main">
        <div class="container">
            <div ng-view></div>
        </div>
    </div>

    <div class="footer" role="contentinfo">
        TeloTraigodeMiPueblo corresponde al Trabajo Fin de Grado de Jorge Aguilera (@jagedn) para la UOC
                 y se encuentra en fase de desarrollo.
    </div>

    <div id="spinner" class="spinner" style="display:none;">
        <g:message code="spinner.alt" default="Loading&hellip;"/>
    </div>

    <asset:javascript src="/telotraigodemipueblo/telotraigodemipueblo.js" />
</body>
</html>
