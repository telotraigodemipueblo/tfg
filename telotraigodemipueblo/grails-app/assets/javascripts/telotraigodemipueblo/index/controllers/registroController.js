//= wrapped

angular
    .module("telotraigodemipueblo.index")
    .controller("RegistroController", RegistroController);

function RegistroController($log,Registration,Notification,$location) {
    var vm = this;

    vm.master = {};

    vm.user = {};

    vm.registrarme = function(form){
        if( vm.user.password !== vm.user.password2 ){
            form.password2.$error.doesntMatch=true;
            return;
        }

        var newUser = new Registration(vm.user);
        newUser.$save(function(data){
            $log.info(data);
            Notification.success("Usuario registrado correctamente");
            $location.url('/index/loginform')
        }, function(error){
            Notification.error("Error "+error.data.status+". "+error.data.message);
            $log.info(error);
        });
    };

}
