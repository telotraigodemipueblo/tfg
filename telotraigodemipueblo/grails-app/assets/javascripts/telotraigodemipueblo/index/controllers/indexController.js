//= wrapped

angular
    .module("telotraigodemipueblo.index")
    .controller("IndexController", IndexController);

function IndexController($rootScope, $scope, $window, $location, $http, contextPath, Notification) {
    var vm = this;

    vm.contextPath = contextPath;

    $rootScope.authenticated = false;

    $http.post('/api/validate').then(function (response) {
        $rootScope.authenticated = true;
        $location.url('/index/welcome');
    }, function(error){
        $rootScope.authenticated = false;
        $location.url('/index');
    });

    vm.identificarme = function(){
        $location.url('/index/loginform');
    }

    vm.registrarme = function(){
        $location.url('/index/registro');
    }

    vm.salir = function(){
        $rootScope.authenticated = false;
        $window.sessionStorage.removeItem('token');
        $window.sessionStorage.removeItem('username');
        $window.sessionStorage.removeItem('roles');
        $location.url('/index');
    }

    $scope.$on('$routeChangeStart', function(angularEvent, newUrl) {
        if (newUrl.requireAuth && !$rootScope.authenticated ) {
            // User isn’t authenticated
            vm.identificarme();
        }

    });
}
