//= wrapped

angular
    .module("telotraigodemipueblo.core")
    .factory("Producto", Producto);

function Producto($resource) {
    var Producto = $resource(
        "/api/conoce/:idLugar/productos/:id",
        {"id": "@id", "idLugar":"@idLugar"},
        {"update": {method: "PUT"}, "list": {method: "GET", isArray: true}}
    );
    return Producto;
}
