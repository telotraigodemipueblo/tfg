//= wrapped

angular
    .module("telotraigodemipueblo.core")
    .factory("Viaje", Viaje);

function Viaje($resource) {
    var Viaje = $resource(
        "/api/conoce/:idLugar/viajes/:id",
        {"id": "@id", "idLugar":"@idLugar"},
        {"update": {method: "PUT"}, "list": {method: "GET", isArray: true}}
    );
    return Viaje;
}
