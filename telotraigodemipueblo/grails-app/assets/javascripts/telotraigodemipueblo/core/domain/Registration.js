//= wrapped

angular
    .module("telotraigodemipueblo.core")
    .factory("Registration", Registration);

function Registration($resource) {
    var Registration = $resource(
        "registration/:id",
        {"id": "@id"},
        {"update": {method: "PUT"}, "list": {method: "GET", isArray: true}}
    );
    return Registration;
}
