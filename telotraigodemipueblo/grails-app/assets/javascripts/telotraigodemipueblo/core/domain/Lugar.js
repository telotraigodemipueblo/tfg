//= wrapped

angular
    .module("telotraigodemipueblo.core")
    .factory("Lugar", Lugar);

function Lugar($resource) {
    var Lugar = $resource(
        "/api/conoce/:id",
        {"id": "@id"},
        {"update": {method: "PUT"}, "list": {method: "GET", isArray: true}}
    );
    return Lugar;
}
