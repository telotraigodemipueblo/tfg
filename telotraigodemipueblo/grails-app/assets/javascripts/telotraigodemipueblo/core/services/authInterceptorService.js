//= wrapped

angular
    .module("telotraigodemipueblo.core")
    .factory("authInterceptorService", authInterceptorService);

function authInterceptorService($rootScope, $window, $location, $q) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.sessionStorage.token) {
                config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
            }
            return config;
        },
         responseError: function(response){
             if( response.status == 401){
              $location.url('/index/landingpage');
             }
             return $q.reject(response);
         }
    };
}