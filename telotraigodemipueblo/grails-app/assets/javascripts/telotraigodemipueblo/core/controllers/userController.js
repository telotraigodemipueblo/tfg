//= wrapped

angular
    .module("telotraigodemipueblo.core")
    .controller("UserController", UserController);

function UserController($window,$http) {
    var vm = this;

    $http.get('/api/me').then(function (response) {
        vm.username = $window.sessionStorage.username;
        vm.roles = $window.sessionStorage.roles;
    });
}
