//= wrapped

angular
    .module("telotraigodemipueblo.network")
    .factory("Search", Search);

function Search($resource) {
    var Search = $resource(
        "/api/search/:id",
        {"id": "@id"},
        {"list": {method: "GET", isArray: true}}
    );
    return Search;
}
