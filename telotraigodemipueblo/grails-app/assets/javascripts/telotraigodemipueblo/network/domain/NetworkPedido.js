//= wrapped

angular
    .module("telotraigodemipueblo.network")
    .factory("NetworkPedido", NetworkPedido);

function NetworkPedido($resource) {
    var NetworkPedido= $resource(
        "/api/viajes/:idViaje/pedidos/:id",
        {"idViaje": "@idViaje", "id":"@id"},
        {"update": {method: "PUT"}, "list": {method: "GET", isArray: true}}
    );
    return NetworkPedido;
}
