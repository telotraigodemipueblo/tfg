//= wrapped

angular
    .module("telotraigodemipueblo.network")
    .controller("NetworkViajesController", NetworkViajesController);

function NetworkViajesController($log, NetworkViaje, NetworkLugar, NetworkPedido, Notification, $location) {
    var vm = this;

    vm.list = NetworkViaje.list();
    vm.viaje = null;

    vm.infoViaje = function( u ){
        vm.viaje = u;
        vm.lugar = NetworkLugar.get({
            id:this.viaje.lugar.id
        })
    }

    vm.realizarPedido = function(t){
        var pedido = new NetworkPedido({
            idViaje : vm.viaje.id,
            producto: t.id,
            cantidad : t.cantidad
        });
        pedido.$save({}, function (response) {
            Notification.success('Solicitud de pedido enviada');
            vm.viaje=null;
        }, function(error){
            Notification.error("Error enviando solicitud")
        });
    }
}
