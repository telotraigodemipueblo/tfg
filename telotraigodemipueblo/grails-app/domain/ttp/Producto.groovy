package ttp

import grails.converters.JSON
import grails.persistence.Entity

@Entity
class Producto {

    String texto
    double precio

    static mapWith = "neo4j"

    static belongsTo = [lugar : Lugar]

    static constraints = {
        texto   nullable: false, maxLength:500
    }

    static mapping = {
        dynamicAssociations true
    }

}
