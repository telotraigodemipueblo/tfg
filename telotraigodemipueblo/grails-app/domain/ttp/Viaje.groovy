package ttp
import grails.persistence.Entity

@Entity
class Viaje {

    String  texto
    Date    fechatope
    Date    vuelvo

    static belongsTo = [ lugar : Lugar]

    static hasMany = [pedidos:Pedido]

    static mapWith = "neo4j"

    static constraints = {
        texto   nullable: false, maxLength:500
        fechatope nullable: false
        vuelvo  nullable: false
    }

    static mapping = {
    }
}
