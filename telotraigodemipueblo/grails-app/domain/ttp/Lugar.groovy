package ttp
import grails.persistence.Entity

@Entity
class Lugar {

    String nombre
    float longitud
    float latitud

    static belongsTo = [usuario:Usuario]

    static hasMany = [
            recibe : Viaje,
            tiene : Producto
    ]

    static mapWith = "neo4j"

    static constraints = {
        nombre nullable:false
    }

    static mapping = {
        nombre index:true
    }
}
