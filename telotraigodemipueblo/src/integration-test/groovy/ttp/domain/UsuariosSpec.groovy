package ttp.domain


import grails.test.mixin.integration.Integration
import grails.transaction.*
import spock.lang.*
import ttp.Role
import ttp.UserRole
import ttp.Usuario

@Integration
@Rollback
class UsuariosSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def "test create usuario"() {

        given:
            Usuario user = new Usuario(nombre: 'nombre', username: "user1@email.com", password: 'password')
            user.save()

        expect:
            Usuario.count()

            Usuario.cypherStatic('match (u:Usuario {username:{1}}) return u',
                ["user1@email.com".toString()]).toList(Usuario).size()==1

    }

    def "test create friends"() {

        given:
        Usuario user1 = new Usuario(nombre: 'nombre1', username: "${new Date().time}@email.com", password: 'password')
        Usuario user2 = new Usuario(nombre: 'nombre2', username: "${new Date().time}_2@email.com", password: 'password')

        user1.amigos(user2)

        user1.save()
        user2.save()

        expect:
        Usuario.count() > 1
    }

    def "test find my friends"() {

        given:
        long id = new Date().time
        Usuario user1 = new Usuario(nombre: 'nombre1', username: "${id}@email.com", password: 'password')
        Usuario user2 = new Usuario(nombre: 'nombre2', username: "${id}_2@email.com", password: 'password')
        Usuario user3 = new Usuario(nombre: 'nombre2', username: "${id}_3@email.com", password: 'password')

        user1.amigos(user2).amigos(user3)

        user1.save()
        user2.save()
        user3.save()

        expect:
            Usuario.findByUsername("${id}@email.com",[fetch:[confia:'join']]).confia.size()==2

    }

    def "test my network"() {

        given:
        long id = new Date().time
        Usuario user1 = new Usuario(nombre: 'nombre1', username: "${id}@email.com", password: 'password')
        Usuario user2 = new Usuario(nombre: 'nombre2', username: "${id}_2@email.com", password: 'password')
        Usuario user3 = new Usuario(nombre: 'nombre3', username: "${id}_3@email.com", password: 'password')
        Usuario user4 = new Usuario(nombre: 'nombre4', username: "${id}_4@email.com", password: 'password')

        Usuario user5 = new Usuario(nombre: 'nadiemequiere', username: "${id}_5@email.com", password: 'password')

        user1.amigos(user2).amigos(user3)
        user2.amigos(user3)
        user3.amigos(user4)

        user1.save(flush:true)
        user2.save(flush:true)
        user3.save(flush:true)
        user4.save(flush:true)
        user5.save(flush:true)

        expect:

        Usuario.findByUsername("${id}@email.com",[fetch:[confia:'join']]).confia.size()==2

        Usuario.cypherStatic("""match (u:Usuario {username:{1}})-[:CONFIA]->(o:Usuario)
                -[:CONFIA]->(n:Usuario)
                return distinct(n)""",
                ["${id}@email.com".toString()]).toList(Usuario)*.nombre.disjoint([
                        'nombre1','nombre2','nombre3','nombre4'
                ]) == false

    }

    def "test user with roles"(){
        given:
        long id = new Date().time
        Usuario user1 = new Usuario(nombre: 'nombre1', username: "${id}@email.com", password: 'password')
        Role r = new Role("ROLE_TEST")

        when:
        user1.save(flush:true)
        r.save(flush:true)
        UserRole.create(user1,r,true)

        then:
        Usuario.findByUsername("${id}@email.com").authorities.size()==1
        Usuario.findByUsername("${id}@email.com").authorities.first().authority=='ROLE_TEST'
        UserRole.findAllByUser(user1)*.role.size()==1
        UserRole.findAllByUser(user1)*.role.first().authority=='ROLE_TEST'
        user1.authorities.size()==1
        user1.authorities.first().authority=='ROLE_TEST'
    }

}
