package ttp.domain

import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import spock.lang.Specification
import ttp.Lugar
import ttp.Usuario
import ttp.Viaje

@Integration
@Rollback
class ViajesSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def "test create viaje"() {

        given:
        long id = new Date().time
        Usuario user = new Usuario(nombre: 'nombre', username: "${id}@email.com", password: 'password')

        Lugar lugar = new Lugar(nombre:"$id",longitud:1,latitud:2)
        user.addToConoce(lugar)

        Viaje viaje = new Viaje(texto:'con la fregoneta', fechatope: new Date()+3, vuelvo:new Date()+10)

        lugar.addToRecibe(viaje)

        user.save(flush:true)

        expect:
        Usuario.findByUsername("${id}@email.com",[fetch:[viaja:'join']]).conoce[0].recibe.size()==1
        Lugar.findByNombre("$id")
        Lugar.findByNombre("$id").recibe.size()==1
    }


    def "test ver viajes de mi amigo"() {

        given:
        long id = new Date().time

        Usuario user, user1

        user = new Usuario(nombre: 'nombre', username: "${id}@email.com", password: 'password')
        user.save(flush: true)

        user1 = new Usuario(nombre: 'nombre1', username: "${id + 1}@email.com", password: 'password')
        user.amigos(user1)
        user.save(flush:true)
        user1.save(flush:true)


        Lugar lugar = new Lugar(nombre: 'mi lugar', longitud: 1, latitud: 2)
        user.addToConoce(lugar)
        Viaje viaje = new Viaje(texto: 'con la fregoneta', fechatope: new Date() + 3, vuelvo: new Date() + 10)
        lugar.addToRecibe(viaje)

        lugar.save(flush:true)
        user.save(flush:true)

        expect:
        Viaje.cypherStatic('MATCH (Usuario {username:{1}})<-[:USUARIO]-(Lugar)<-[:LUGAR]-(v:Viaje) return v',
                ["${id}@email.com".toString()]).size()==1

        Viaje.cypherStatic('MATCH (u:Usuario {username:{1}})-[:CONFIA]->(o:Usuario)<-[:USUARIO]-(l:Lugar)<-[:LUGAR]-(v:Viaje) return v',
                ["${id+1}@email.com".toString()]).size() == 1

        Viaje.cypherStatic('MATCH (u:Usuario {username:{1}})-[:CONFIA]->(o:Usuario)<-[:USUARIO]-(l:Lugar)<-[:LUGAR]-(v:Viaje) return v',
                ["${id+1}@email.com".toString()]).toList(Viaje)[0].texto == 'con la fregoneta'

    }

    def "test ver viajes de varios de mis amigos"() {

        given:
        long id = new Date().time
        Usuario user = new Usuario(nombre: 'nombre', username: "${id}@email.com", password: 'password')
        user.save(flush:true)

        Usuario user1 = new Usuario(nombre: 'nombre1', username: "${id+1}@email.com", password: 'password')
        user1.save(flush:true)

        Usuario user2 = new Usuario(nombre: 'nombre2', username: "${id+2}@email.com", password: 'password')
        user2.save(flush:true)

        user.amigos(user1).amigos(user2)
        user1.amigos(user2)
        user.save(flush:true)
        user1.save(flush:true)
        user2.save(flush:true)

        Lugar lugar = new Lugar(nombre:'mi lugar',longitud:1,latitud:2)
        user.addToConoce(lugar)
        Viaje viaje = new Viaje(texto:'con la fregoneta', fechatope: new Date()+3, vuelvo:new Date()+10)
        lugar.addToRecibe(viaje)
        user.save(flush:true)

        Lugar lugar1 = new Lugar(nombre:'otro lugar',longitud:1,latitud:2)
        user1.addToConoce(lugar1)
        Viaje viaje1 = new Viaje(texto:'con el smart', fechatope: new Date()+3, vuelvo:new Date()+10)
        lugar1.addToRecibe(viaje1)
        user1.save(flush:true)

        expect:
        Viaje.cypherStatic('MATCH (u:Usuario {username:{1}})-[:CONFIA]->(o:Usuario)<-[:USUARIO]-(l:Lugar)<-[:LUGAR]-(v:Viaje) return v',
                ["${id+2}@email.com".toString()]).toList(Viaje).size() == 2
    }

}
