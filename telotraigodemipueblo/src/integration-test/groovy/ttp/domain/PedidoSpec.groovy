package ttp.domain

/**
 * Created by jorge on 9/12/16.
 */
import grails.test.mixin.integration.Integration
import grails.transaction.*
import spock.lang.*
import ttp.Lugar
import ttp.Usuario
import ttp.Producto
import ttp.Pedido
import ttp.Viaje

@Integration
@Rollback

class PedidoSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def "test create pedido"() {

        given:
        long id = new Date().time
        Usuario user = new Usuario(nombre: 'nombre', username: "${id}@email.com", password: 'password')

        Lugar lugar = new Lugar(nombre:"$id",longitud:1,latitud:2)
        user.addToConoce(lugar)

        Producto producto = new Producto(texto:'un queso rico', precio:10)

        lugar.addToTiene(producto)

        Viaje viaje = new Viaje(texto:'viaje dummy',fechatope:new Date()+2,vuelvo:new Date()+10)
        lugar.addToRecibe(viaje)

        user.save(flush:true)

        expect:
        Usuario.findByUsername("${id}@email.com")
        Lugar.findByNombre("$id")
        Lugar.findByNombre("$id").tiene.size()==1
    }
}
