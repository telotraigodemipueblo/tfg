package ttp.domain


import grails.test.mixin.integration.Integration
import grails.transaction.*
import spock.lang.*
import ttp.Lugar
import ttp.Usuario

@Integration
@Rollback
class LugaresSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def "test create lugar"() {

        given:
        long id = new Date().time
        Usuario user = new Usuario(nombre: 'nombre', username: "${id}@email.com", password: 'password')

        Lugar lugar = new Lugar(nombre:'mi lugar',longitud:1,latitud:2)
        user.addToConoce(lugar)
        user.save()

        expect:
        Usuario.findByUsername("${id}@email.com",[fetch:[conoce:'join']]).conoce.size()==1
    }

}
