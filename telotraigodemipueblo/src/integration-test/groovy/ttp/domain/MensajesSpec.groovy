package ttp.domain

import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import spock.lang.Specification
import ttp.Mensaje
import ttp.Usuario

@Integration
@Rollback
class MensajesSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    def "test create mensaje"() {

        given:
        long id = new Date().time

        Usuario user = new Usuario(nombre: 'nombre', username: "${id}@email.com", password: 'password')
        user.save()

        Usuario user1 = new Usuario(nombre: 'nombre', username: "${id+1}@email.com", password: 'password')
        user1.save()

        Mensaje m = new Mensaje(
                texto: 'hola ke ase 1',
                enviado: new Date(),
                leido:null,
                remitente:user
        )
        m.addToDestinatarios(user1)
        m.save(flush:true)

        expect:
        Mensaje.count()
        (Mensaje.cypherStatic("MATCH (m:Mensaje) WHERE ( m.texto={1} ) return m",['hola ke ase 1']) as
            Mensaje).texto == 'hola ke ase 1'
    }

    def "mensajes no leidos"() {

        given:
        long id = new Date().time

        Usuario user = new Usuario(nombre: 'nombre', username: "${id}@email.com", password: 'password')
        user.save()

        Usuario user1 = new Usuario(nombre: 'nombre', username: "${id+1}@email.com", password: 'password')
        user1.save()

        Mensaje m = new Mensaje(
                texto: 'hola ke ase 2',
                enviado: new Date(),
                remitente:user
        )
        m.addToDestinatarios(user1)
        m.save(flush:true)

        Mensaje m1 = new Mensaje(
                texto: 'hola ke ase 3',
                enviado: new Date(),
                remitente:user1
        )
        m1.addToDestinatarios(user)
        m1.save(flush:true)

        expect:
        Mensaje.findAll("MATCH (m:Mensaje) where m.leido is null return m").size()
    }


    def "respondiendo mensaje"() {

        given:
        long id = new Date().time

        Usuario user = new Usuario(nombre: 'nombre', username: "${id}@email.com", password: 'password')
        user.save()

        Usuario user1 = new Usuario(nombre: 'nombre', username: "${id+1}@email.com", password: 'password')
        user1.save()

        Mensaje m = new Mensaje(
                texto: 'hola ke ase 4',
                enviado: new Date(),
                remitente:user
        )
        m.addToDestinatarios(user1)
        m.save(flush:true)

        Mensaje m1 = new Mensaje(
                texto: 'poca cosa',
                enviado: new Date(),
                remitente:user1
        )
        m1.addToDestinatarios(user).responde(m)
        m1.save(flush:true)

        expect:
        Mensaje.find("MATCH (m:Mensaje)-[:RESPONDE]->(n:Mensaje {texto:{1}}) return m",'hola ke ase 4').texto=='poca cosa'
        Mensaje.find("""MATCH (m:Mensaje)-[:RESPONDE]->
                            (n:Mensaje)-[:DESTINATARIOS]->
                            (u:Usuario {username:{1}}) return m""","${id+1}@email.com".toString()).texto=='poca cosa'
    }
}
