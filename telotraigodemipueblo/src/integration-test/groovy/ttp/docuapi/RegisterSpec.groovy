package ttp.docuapi

import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import groovy.json.JsonBuilder
import org.junit.Rule
import org.junit.rules.TestName
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import spock.lang.Shared
import ttp.UserRole
import ttp.Usuario

import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.hamcrest.CoreMatchers.not

/**
 * Created by jorge on 22/10/16.
 */
@Integration
@Rollback
class RegisterSpec extends SpecificationBase{

    @Shared String name = Random.newInstance().nextInt().toString()

    void "register username with password"() {
        expect: "dado un perfil que no existe podamos registralo"
        givenRequest( documentBase("register",
                requestFields(FieldsDescriptors.requestRegisterFields),responseFields(FieldsDescriptors.responseRegisterFields)))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .accept(MediaType.APPLICATION_JSON.toString())
                .body( new JsonBuilder([nombre:name, username:"$name@test.com",password:name]).toPrettyString() )
                .when()
                .post("/registration")
                .then()
                .assertThat()
                .statusCode(201)
    }

    void "existing username with password"() {
        expect: "dado un perfil que ya existe, no podamos registrarlo de nuevo"
        givenRequest( documentBase("invalidregister") )
                .contentType(MediaType.APPLICATION_JSON.toString())
                .accept(MediaType.APPLICATION_JSON.toString())
                .body( new JsonBuilder([nombre:name, username:"$name@test.com",password:name]).toPrettyString() )
                .when()
                .post("/registration")
                .then()
                .assertThat()
                .statusCode(not(201))
    }

}
