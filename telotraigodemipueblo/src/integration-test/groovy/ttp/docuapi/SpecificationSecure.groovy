package ttp.docuapi

import com.jayway.restassured.builder.RequestSpecBuilder
import grails.plugins.rest.client.RestBuilder
import groovy.json.JsonBuilder
import org.junit.Rule
import org.junit.rules.TestName
import org.springframework.http.HttpStatus
import spock.lang.Shared
import ttp.Lugar
import ttp.Producto
import ttp.Role
import ttp.UserRole
import ttp.Usuario
import ttp.Viaje

import static org.springframework.restdocs.restassured.RestAssuredRestDocumentation.documentationConfiguration

/**
 * Created by jorge on 6/11/16.
 */
class SpecificationSecure extends SpecificationBase{

    @Rule TestName testName = new TestName()

    @Shared String name= Random.newInstance().nextInt().toString()
    @Shared String username = "$name@test.com"
    @Shared String password= "password"

    @Shared String friendPassword= "password"

    @Shared Usuario usuario, friend

    @Shared boolean firstSpec = true
    @Shared boolean lastSpec = false

    boolean friendRequired = false

    @Shared String lugarname= Random.newInstance().nextInt().toString()
    boolean lugarRequired = false
    @Shared Lugar sharedLugar

    boolean viajeRequired = false
    @Shared Viaje sharedViaje

    @Shared Producto sharedProducto
    boolean productoRequired = false

    void setup() {
        if( !firstSpec )
            return

        firstSpec=false
        Usuario.withTransaction {
            Role role = Role.findByAuthority('ROLE_USER')
            usuario = new Usuario(nombre: name, username: username, password: password)
            usuario.save(flush: true)
            UserRole.create(usuario, role, true)

            if( friendRequired ){
                friend = new Usuario(nombre: name+"_friend", username: name+"_friend@test.com", password: password)
                UserRole.create(friend, role, true)
                usuario.amigos(friend)
                usuario.save(flush: true)
                friend.save(flush: true)
            }

            if( lugarRequired ){
                sharedLugar = new Lugar(nombre:'mi lugar',longitud:1,latitud:2)

                if( viajeRequired ){
                    sharedViaje = new Viaje(texto:'con la fregoneta', fechatope: new Date()+3, vuelvo:new Date()+10)
                    sharedLugar.addToRecibe(sharedViaje)
                    sharedLugar.save(flush:true)
                }

                if( productoRequired ){
                    sharedProducto = new Producto(texto:'queso rico', precio: 10)
                    sharedLugar.addToTiene(sharedProducto)
                    sharedLugar.save(flush:true)
                }

                usuario.addToConoce(sharedLugar)
                usuario.save(flush: true)
            }

        }

    }

    void cleanup(){
        if( !lastSpec )
            return

        Usuario.withTransaction{
            if( friendRequired ){
                UserRole.removeAll(friend,true)
                friend.delete(flush:true)
                friend = null
            }

            if( lugarRequired ){
                sharedLugar.recibe*.delete(flush:true)
                sharedLugar.delete(flush:true)
            }

            UserRole.removeAll(usuario,true)
            //usuario.conoce*.delete(flush:true)
            usuario.delete(flush:true)
            usuario = null
        }
    }

    String obtainAccessToken( username, password ){
        def jsonContent = new JsonBuilder([username: username, password: password]).toPrettyString()
        def rest = new RestBuilder()
        def response = rest.post("http://localhost:${serverPort}/api/login"){
            header("Content-Type", "application/json");
            body(jsonContent)
        }
        assert response.statusCode == HttpStatus.OK
        response.json.access_token
    }

    String getAccessToken(){
        obtainAccessToken(username, password)
    }

    String getFriendAccessToken(){
        obtainAccessToken(friend.username, friendPassword)
    }

}
