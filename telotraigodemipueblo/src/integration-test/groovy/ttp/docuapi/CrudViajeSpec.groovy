package ttp.docuapi

import grails.test.mixin.integration.Integration
import groovy.json.JsonBuilder
import org.springframework.http.MediaType
import org.springframework.restdocs.request.ParameterDescriptor
import spock.lang.Shared
import ttp.Lugar

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters

/**
 * Created by jorge on 29/11/16.
 */
@Integration
class CrudViajeSpec extends SpecificationSecure {

    @Shared Long idViaje

    @Override
    boolean isLugarRequired() {
        return true
    }


    void "get Viajes de un lugar vacio"(){
        given: "dado un usuario logeado y un lugar"

        def then = givenRequest(documentBase("crud/viaje/empty",
                pathParameters(FieldsDescriptors.paramLugarId)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .get("/api/conoce/{idLugar}/viajes",sharedLugar.id)
                .then()

        expect: "no tiene Viajes"
        then.assertThat().statusCode(200)
        //then.extract().path('[0]') == null
    }

    void "create Viaje #viaje con #tope #vuelvo"(){
        given: "un Viaje a crear"

        def then = givenRequest(documentBase("crud/viaje/create",
                requestFields(FieldsDescriptors.requestCreateViajeFields),
                pathParameters(FieldsDescriptors.paramLugarId),
                responseFields(FieldsDescriptors.responseViajeFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .body( new JsonBuilder([texto:viaje,
                                        fechatope:(new Date()+tope).time,
                                        vuelvo:(new Date()+vuelvo).time]).toPrettyString() )
                .when()
                .post("/api/conoce/{idLugar}/viajes",sharedLugar.id)
                .then()

        expect: "hay al menos un Viaje"

        then.assertThat().statusCode(201)

        assert (idViaje=then.extract().path('id') as Long) != 0

        where:
        viaje           | tope | vuelvo
        "eiii que me voy" | 3 | 10
    }

    void "get Viajes de un lugar "(){
        given: "dado un usuario logeado y un lugar con Viajes"

        def then = givenRequest(documentBase("crud/viaje/list",
                pathParameters(FieldsDescriptors.paramLugarId),
                responseFields(fieldWithPath("[]").description("Un array de viajes"))
                        .andWithPrefix('[].', FieldsDescriptors.responseViajeFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .get("/api/conoce/{idLugar}/viajes",sharedLugar.id)
                .then()

        expect: "tiene Viajes"
        then.assertThat().statusCode(200)
        then.extract().path('[0].id')
    }

    void "get Viaje del usuario "(){
        given: "dado un usuario logeado"

        def then = givenRequest(documentBase("crud/viaje/get",
                pathParameters([FieldsDescriptors.paramLugarId,FieldsDescriptors.paramId,] as ParameterDescriptor[]),
                responseFields(FieldsDescriptors.responseViajeFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .get("/api/conoce/{idLugar}/viajes/{id}", sharedLugar.id, idViaje)
                .then()

        expect: "tiene un Viaje creado y es el mismo que creamos"
        then.assertThat().statusCode(200)
        idViaje == then.extract().path('id') as Long
    }

    void "update Viaje del usuario "(){

        given: "dado un usuario logeado"

        def then = givenRequest(documentBase("crud/viaje/update",
                pathParameters([FieldsDescriptors.paramLugarId,FieldsDescriptors.paramId,] as ParameterDescriptor[]),
                responseFields(FieldsDescriptors.responseViajeFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .body( new JsonBuilder([texto:'texto actualizado']).toPrettyString() )
                .when()
                .put("/api/conoce/{idLugar}/viajes/{id}",sharedLugar.id, idViaje)
                .then()

        expect: "tiene un lugar creado y es el mismo que creamos"
        then.assertThat().statusCode(200)
        idViaje == then.extract().path('id') as Long
        'texto actualizado' == then.extract().path('texto')
    }

    void "delete Viaje del usuario "(){
        lastSpec=true

        given: "dado un usuario logeado con un Viaje"

        def then = givenRequest(documentBase("crud/viaje/delete",
                pathParameters([FieldsDescriptors.paramLugarId,FieldsDescriptors.paramId,] as ParameterDescriptor[]),
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .delete("/api/conoce/{idLugar}/viajes/{id}",sharedLugar.id, idViaje)
                .then()

        expect: "que se puede borrar el lugar"
        then.assertThat().statusCode(204)
    }
}
