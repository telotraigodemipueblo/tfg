package ttp.docuapi


import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import groovy.json.JsonBuilder
import org.springframework.http.MediaType
import org.springframework.restdocs.request.ParameterDescriptor
import spock.lang.Shared
import ttp.Lugar
import ttp.Mensaje

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters

/**
 * Created by jorge on 4/11/16.
 */
@Integration
class CrudLugaresSpec extends SpecificationSecure {

    @Shared Long id

    void setup(){
    }

    void cleanup(){
        if( lastSpec ){
            Lugar.withTransaction{
                Lugar.findByName(lugarname)?.delete(flush:true)
            }
        }
    }

    void "get lugares del usuario sin lugares"(){
        given: "dado un usuario logeado"

        def then = givenRequest(documentBase("crud/lugar/empty",))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .get("/api/conoce")
                .then()

        expect: "no tiene lugares"
        then.assertThat().statusCode(200)
        then.extract().path('[0]') == null
    }

    void "create lugar #lugar"(){
        given: "un lugar a crear"

        def then = givenRequest(documentBase("crud/lugar/create",
                requestFields(FieldsDescriptors.requestCreateLugarFields),
                responseFields(FieldsDescriptors.responseLugarFields)
                        .andWithPrefix('recibe',fieldWithPath('[]').description('Un array de viajes').type(List.class).optional())
                        //.andWithPrefix('recibe[].',FieldsDescriptors.responseViajeFields)
                        .andWithPrefix('tiene',fieldWithPath('[]').description('Un array de productos').type(List.class).optional())
                        //.andWithPrefix('tiene[].',FieldsDescriptors.responseProductoFields)
                        .andWithPrefix('usuario.',FieldsDescriptors.responseViajeUsuarioFields)

        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .body( new JsonBuilder([nombre:lugarname,latitud:1,longitud:10]).toPrettyString() )
                .when()
                .post("/api/conoce")
                .then()

        expect: "hay al menos un lugar"

        then.assertThat().statusCode(201)

        assert (id=then.extract().path('id') as Long) != 0
    }

    void "get lugares del usuario "(){
        given: "dado un usuario logeado"

        def then = givenRequest(documentBase("crud/lugar/list",
                responseFields(fieldWithPath("[]").description("Un array de lugares")
                ).andWithPrefix('[].', FieldsDescriptors.responseLugarFields)))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .get("/api/conoce")
                .then()

        expect: "tiene un lugar creado y es el mismo que creamos"
                then.assertThat().statusCode(200)
                id == then.extract().path('[0].id') as Long
    }

    void "get lugar del usuario "(){
        given: "dado un usuario logeado"

        def then = givenRequest(documentBase("crud/lugar/get",
                pathParameters([FieldsDescriptors.paramId] as ParameterDescriptor[]),
                responseFields(FieldsDescriptors.responseLugarFields)
                        .andWithPrefix('usuario.',FieldsDescriptors.responseViajeUsuarioFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .get("/api/conoce/{id}", id)
                .then()

        expect: "tiene un lugar creado y es el mismo que creamos"
        then.assertThat().statusCode(200)
        id == then.extract().path('id') as Long
    }


    void "update lugar del usuario "(){

        given: "dado un usuario logeado"

        def then = givenRequest(documentBase("crud/lugar/update",
                pathParameters([FieldsDescriptors.paramId] as ParameterDescriptor[]),
                responseFields(FieldsDescriptors.responseLugarFields)
                        .andWithPrefix('usuario.',FieldsDescriptors.responseViajeUsuarioFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .body( new JsonBuilder([nombre:lugarname,latitud:100,longitud:200]).toPrettyString() )
                .when()
                .put("/api/conoce/{id}",id)
                .then()

        expect: "tiene un lugar creado y es el mismo que creamos"
        then.assertThat().statusCode(200)
        id == then.extract().path('id') as Long
        100 == then.extract().path('latitud') as int
    }

    void "delete lugar del usuario "(){
        lastSpec=true

        given: "dado un usuario logeado con un lugar"

        def then = givenRequest(documentBase("crud/lugar/delete",
                pathParameters([FieldsDescriptors.paramId] as ParameterDescriptor[]),
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .delete("/api/conoce/{id}",id)
                .then()

        expect: "que se puede borrar el lugar"
        then.assertThat().statusCode(204)
    }

}
