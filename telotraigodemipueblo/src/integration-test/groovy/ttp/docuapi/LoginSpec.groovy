package ttp.docuapi

import grails.core.GrailsApplication
import grails.test.mixin.integration.Integration
import groovy.json.JsonBuilder
import org.junit.Rule
import org.junit.rules.TestName
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.restdocs.headers.HeaderDocumentation
import org.springframework.test.annotation.DirtiesContext
import spock.lang.Shared
import ttp.Usuario

import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders

/**
 * Created by jorge on 24/10/16.
 */
@Integration
class LoginSpec extends SpecificationSecure{

    void 'login user'() {
        given: "unas credenciales validas"
            def request = givenRequest(documentBase("login",
                requestFields(FieldsDescriptors.requestLoginFields),responseFields(FieldsDescriptors.responseLoginFields)))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .content( new JsonBuilder([username: username, password: password]).toPrettyString() )
                .when()

        when: "solicitamos un login"
            def then = request.post("/api/login").then()

        then: "el codigo de retorno es 200"
                then.assertThat()
                .statusCode(200)
    }

    void 'invalid user'() {
        given: "unas credenciales invalidas"
            def request = givenRequest(documentBase("invalidlogin",
                requestFields(FieldsDescriptors.requestLoginFields)))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .content( new JsonBuilder([username: username, password: password.reverse()]).toPrettyString() )
                .when()

        when: "solicitamos un login"
            def then = request.post("/api/login").then()

        then: "el codigo de retorno es 401"
                then.assertThat()
                .statusCode(401)
    }

    void 'validate user'(){
        lastSpec=true

        expect: "que la validacion sea correcta"
        givenRequest(documentBase("validate_user",
                requestHeaders(FieldsDescriptors.requestSecureHeaders),
                responseFields(FieldsDescriptors.responseValidateLoginFields)))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .post("/api/validate")
                .then()
                .assertThat()
                .statusCode(200)
    }

}
