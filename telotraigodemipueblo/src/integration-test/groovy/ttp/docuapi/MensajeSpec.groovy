package ttp.docuapi
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.http.MediaType
import ttp.Mensaje

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders

/**
 * Created by jorge on 4/11/16.
 */
@Integration
@Rollback
class MensajeSpec extends SpecificationSecure {

    @Override
    boolean isFriendRequired() {
        return true
    }

    Mensaje m
    void setup(){
        m = new Mensaje(texto:'hola', enviado:new Date(), remitente: friend)
        m.addToDestinatarios(usuario)
        assert m.save(flush:true)
    }

    void cleanup(){
        m.delete(flush:true)
    }

    void "search #max by #search like #like"(){
        lastSpec=true
        given: "un mensaje en mi buzon"

        when: "request la list de mensajes"
        def when = givenRequest(documentBase("mensajes",
                requestHeaders(FieldsDescriptors.requestSecureHeaders),
                requestParameters(FieldsDescriptors.requestMessagesFields),
                responseFields(fieldWithPath('[]').description('Un array de mensajes').type(List.class).optional())
                        .andWithPrefix('[].destinatarios',FieldsDescriptors.responseUsersFields)
                        .andWithPrefix('[].remitente',FieldsDescriptors.responseUsersFields)
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .when()
                .get("/api/mensajes?onlynews=false&max=10")

        then: "hay 1 mensaje en mi buzon"
        when.then()
                .assertThat()
                .statusCode(200)

    }

}
