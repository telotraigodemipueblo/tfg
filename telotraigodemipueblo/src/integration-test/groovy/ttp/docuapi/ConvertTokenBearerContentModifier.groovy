package ttp.docuapi

import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.springframework.http.MediaType
import org.springframework.restdocs.operation.preprocess.ContentModifier
import org.springframework.restdocs.operation.preprocess.ContentModifyingOperationPreprocessor
import org.springframework.restdocs.operation.preprocess.OperationPreprocessor
import org.springframework.restdocs.operation.preprocess.Preprocessors
import org.springframework.restdocs.operation.preprocess.PrettyPrintingContentModifier

/**
 * Created by jorge on 15/11/16.
 */
class ConvertTokenBearerContentModifier implements ContentModifier{

    byte[] modifyContent(byte[] originalContent, MediaType contentType){

        if( contentType != MediaType.APPLICATION_JSON_UTF8)
            return originalContent

        def json = new JsonSlurper().parse(originalContent)
        ['Authorization','access_token','refresh_token'].each { key ->
            if( json instanceof List ) {
                json.each{ j->
                    if( j[key] ) {
                        String value = j."$key"
                        value = value[0..Math.min(20, value.length())] + '....'
                        j[key] = value
                    }
                }
            }else{
                if( json[key] ) {
                    String value = json."$key"
                    value = value[0..Math.min(20, value.length())] + '....'
                    json[key] = value
                }
            }
        }
        JsonOutput.toJson(json)
    }

    public static OperationPreprocessor convertTokenBearerContentModifier(){
        new ContentModifyingOperationPreprocessor(
                new ConvertTokenBearerContentModifier());
    }
}
