package ttp.docuapi

import org.springframework.restdocs.headers.HeaderDescriptor
import org.springframework.restdocs.headers.HeaderDocumentation
import org.springframework.restdocs.payload.FieldDescriptor
import org.springframework.restdocs.request.ParameterDescriptor
import org.springframework.restdocs.request.PathParametersSnippet
import ttp.Usuario

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName

/**
 * Created by jorge on 26/10/16.
 */
class FieldsDescriptors {

    static HeaderDescriptor[] requestSecureHeaders=[
        headerWithName("Authorization").description("Bearer auth credentials")
    ]

    static FieldDescriptor[] requestIdFields = [
            fieldWithPath('id').description('el id del recurso'),
    ]

    static ParameterDescriptor paramId = parameterWithName("id").description("el Id del recurso")


    static ParameterDescriptor paramLugarId = parameterWithName("idLugar").description("el Id del lugar")

    static ParameterDescriptor paramViajeId = parameterWithName("idViaje").description("el Id del viaje")

    static FieldDescriptor[] requestRegisterFields = [
            fieldWithPath('nombre').description('el nombre del usuario'),
            fieldWithPath('username').description('el identificado de usuario'),
            fieldWithPath('password').description('la clave del usuario'),
    ]

    static FieldDescriptor[] responseRegisterFields = [
            fieldWithPath('timestamp').description('el instante en el que se crea el usuario'),
            fieldWithPath('status').description('codigo de estado'),
            fieldWithPath('error').description('mensaje de error'),
            fieldWithPath('message').description('mensaje de resultado'),
            fieldWithPath('path').description('ruta solicitada'),
    ]

    static FieldDescriptor[] responseRegisterExistingFields = [
            fieldWithPath('timestamp').description('el instante en el que se crea el usuario'),
            fieldWithPath('status').description('codigo de estado'),
            fieldWithPath('error').description('mensaje de error'),
            fieldWithPath('message').description('mensaje de resultado'),
            fieldWithPath('path').description('ruta solicitada'),
    ]

    static FieldDescriptor[] requestLoginFields = [
            fieldWithPath('username').description('el codigo de usuario'),
            fieldWithPath('password').description('la clave del usuario'),
    ]

    static FieldDescriptor[] responseLoginFields = [
            fieldWithPath('username').description('el codigo de usuario'),
            fieldWithPath('roles').description('Lista de roles'),
            fieldWithPath('token_type').description('Bearer'),
            fieldWithPath('access_token').description('token a enviar en cada peticion'),
            fieldWithPath('expires_in').description('segundos de validez del token'),
            fieldWithPath('refresh_token').description('token a utilizar para refrescar el access_token'),
    ]

    static FieldDescriptor[] responseValidateLoginFields = [
            fieldWithPath('username').description('el codigo de usuario'),
            fieldWithPath('roles').description('Lista de roles'),
            fieldWithPath('token_type').description('Bearer'),
            fieldWithPath('access_token').description('token a enviar en cada peticion'),
            fieldWithPath('expires_in').description('segundos de validez del token'),
    ]

    static ParameterDescriptor[] requestSearchUsersFields = [
            //parameterWithName('kind').description('Tipo de recurso a buscar'),
            parameterWithName('like').description('Expresión a filtrar'),
            parameterWithName('max').description('maximo de usuarios a recuperar'),
    ]

    static FieldDescriptor[] responseUsersFields = [
            fieldWithPath('id').description('el id del usuario').type(String.class).optional(),
            fieldWithPath('nombre').description('el nombre del usuario').type(String.class).optional(),
            fieldWithPath('username').description('el codigo de usuario').type(String.class).optional(),
    ]

    static ParameterDescriptor[] requestMessagesFields = [
            parameterWithName('onlynews').description('Si true sólo los no leidos'),
            parameterWithName('max').description('maximo mensajes a recuperar'),
    ]

    static FieldDescriptor[] requestInvitationFields = [
            fieldWithPath('id').description('el id del usuario destinatario del mensaje').type(String.class),
    ]

    static FieldDescriptor[] requestInvitationAcceptedFields = [
            fieldWithPath('mensaje').description('el id del usuario destinatario del mensaje').type(String.class),
    ]


    static FieldDescriptor[] responseMessagesFields = [
            fieldWithPath('id').description('el identificador unico del mensaje').type(String.class),
            fieldWithPath('tipo').description('el tipo de mensaje (MENSAJE, INVITACION, PEDIDO, ...)').type(String.class),
            fieldWithPath('texto').description('el texto del mensaje').type(String.class),
            fieldWithPath('enviado').description('la fecha en que fue enviado').type(Date.class),
            fieldWithPath('leido').type(Date.class).description('la fecha en la que fue leido o null si no lo ha sido todavia').optional(),
            fieldWithPath('destinatarios[]').description('un array de usuarios destinatarios').type(Usuario.class).optional(),
            fieldWithPath('remitente').type(Usuario.class).description('el remitente del mensaje').optional(),
    ]

    static FieldDescriptor[] responseIAmFields = [
            fieldWithPath('id').description('el identificador unico del usuario').type(String.class),
            fieldWithPath('nombre').description('el nombre del usuario').type(String.class),
            fieldWithPath('username').description('el alias del usuario').type(String.class),
            //fieldWithPath('conoce[]').description('array de lugares a los que viaja'),
            //fieldWithPath('confia[]').description('array de amigos en los que confia'),
            //fieldWithPath('viaja[]').description('array de viajes a realizar o realizados'),
    ]

    static FieldDescriptor[] requestCreateLugarFields = [
            fieldWithPath('nombre').description('el nombre del lugar').type(String.class),
            fieldWithPath('latitud').description('la latitud').type(Number.class),
            fieldWithPath('longitud').description('la longitud').type(Number.class),
    ]

    static FieldDescriptor[] responseLugarFields = [
            fieldWithPath('id').description('el identificador unico del lugar').type(String.class),
            fieldWithPath('nombre').description('el nombre del lugar').type(String.class),
            fieldWithPath('latitud').description('la latitud').type(Number.class).optional(),
            fieldWithPath('longitud').description('la longitud').type(Number.class).optional(),
            fieldWithPath('recibe').description('una lista de viajes que recibe').type(List.class).optional(),
            fieldWithPath('tiene').description('una lista de productos que tiene').type(List.class).optional(),
    ]

    static FieldDescriptor[] requestCreateProductoFields = [
            fieldWithPath('texto').description('el nombre del producto').type(String.class),
            fieldWithPath('precio').description('el precio al que lo conseguimos').type(Number.class),
    ]

    static FieldDescriptor[] responseProductoFields = [
            fieldWithPath('id').description('el identificador unico del producto').type(String.class),
            fieldWithPath('texto').description('un texto que describe el producto').type(String.class),
            fieldWithPath('precio').description('el precio al que se consigue').type(Number.class),
    ]

    static FieldDescriptor[] requestCreateViajeFields = [
            fieldWithPath('texto').description('una descripcion del viaje').type(String.class),
            fieldWithPath('fechatope').description('feha tope para hacer pedidos').type(Date.class),
            fieldWithPath('vuelvo').description('fecha en la que vuelve del viaje').type(Date.class),
    ]

    static FieldDescriptor[] responseViajeFields = [
            fieldWithPath('id').description('el identificador unico del viaje').type(String.class),
            fieldWithPath('texto').description('un texto que describe el viaje').type(String.class),
            fieldWithPath('fechatope').description('feha tope para hacer pedidos').type(Date.class),
            fieldWithPath('vuelvo').description('fecha en la que vuelve del viaje').type(Date.class),
    ]

    static FieldDescriptor[] responseViajeLugarFields = [
            fieldWithPath('id').description('el identificador unico del lugar').type(String.class),
            fieldWithPath('nombre').description('el nombre del lugar').type(String.class),
    ]


    static FieldDescriptor[] responseViajeUsuarioFields = [
            fieldWithPath('id').description('el id del usuario').type(String.class),
            fieldWithPath('username').description('el codigo de usuario').type(String.class),
            fieldWithPath('nombre').description('el nombre del usuario').type(String.class).optional(),
    ]

    static FieldDescriptor[] responsePedidoFields = [
            fieldWithPath('id').description('el identificador unico del pedido').type(String.class),
            fieldWithPath('cantidad').description('la cantidad que deseo pedir').type(Integer.class),
            fieldWithPath('producto.id').description('el id del producto pedido').type(String.class),
            fieldWithPath('solicitante.id').description('el id del usuario que lo pide').type(Integer.class),
    ]
    static FieldDescriptor[] requestCreatePedidoFields = [
            fieldWithPath('producto').description('el id del producto').type(String.class),
            fieldWithPath('cantidad').description('la cantidad que deseamos').type(Number.class),
    ]

}
