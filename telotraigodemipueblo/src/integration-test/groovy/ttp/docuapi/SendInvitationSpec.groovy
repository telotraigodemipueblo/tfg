package ttp.docuapi

import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import groovy.json.JsonBuilder
import org.springframework.http.MediaType
import org.springframework.restdocs.operation.preprocess.Preprocessors
import spock.lang.Shared
import ttp.Mensaje

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters

/**
 * Created by jorge on 4/11/16.
 */
@Integration
@Rollback
class SendInvitationSpec extends SpecificationSecure {

    @Override
    boolean isFriendRequired() {
        return true
    }

    @Shared Long id
    void cleanup(){
        if( lastSpec ) {
            Mensaje.get(id)?.delete(flush: true)
        }
    }

    void 'send invitation request'(){
        given: "un user y un friend"
        def when = givenRequest(documentBase("invitacion/request",
                requestFields(fieldWithPath("destinatarios").description("Un array de destinatarios"))
                        .andWithPrefix('destinatarios[].', FieldsDescriptors.requestInvitationFields),
                responseFields(FieldsDescriptors.responseMessagesFields),
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${accessToken}")
                .body( new JsonBuilder(
                    destinatarios:[[id:friend.id]]
                 ).toPrettyString() )
                .when()
                .post("/api/invitacion/send")
        def then = when.then()

        expect: "hay 1 mensaje en mi buzon"
        then.assertThat().statusCode(201)
        assert (id = then.extract().path('id') as Long) != 0
        Mensaje.find("MATCH (m:Mensaje)-[:DESTINATARIOS]->(u:Usuario {username:{1}}) return m",friend.username).texto.startsWith('Hola')
    }

    void 'accept invitation request'(){
        lastSpec=true
        given: "una invitacion"
        def when = givenRequest(documentBase("invitacion/accepted",
                requestFields(FieldsDescriptors.requestInvitationAcceptedFields),
                responseFields(FieldsDescriptors.responseMessagesFields),
        ))
                .contentType(MediaType.APPLICATION_JSON.toString())
                .headers("Authorization":"Bearer ${friendAccessToken}")
                .body( new JsonBuilder(mensaje:id).toPrettyString() )
                .when()
                .post("/api/invitacion/accept")
        def then = when.then()

        expect: "hay 1 mensaje en mi buzon"
        then.assertThat().statusCode(200)
        assert ( then.extract().path('id') as Long) == id
        Mensaje.find("MATCH (m:Mensaje)-[:DESTINATARIOS]->(u:Usuario {username:{1}}) return m",friend.username).texto.startsWith('Hola')
        Mensaje.find("MATCH (m:Usuario)-[:CONFIA]->(u:Usuario {username:{1}}) return m",friend.username).username == usuario.username
    }

}
