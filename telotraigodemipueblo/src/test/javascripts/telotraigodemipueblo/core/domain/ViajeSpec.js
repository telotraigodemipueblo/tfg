describe("telotraigodemipueblo.core module", function() {
    var $httpBackend;

    beforeEach(angular.mock.module("telotraigodemipueblo.core", function() {
    }));

    beforeEach(angular.mock.inject(function(_$httpBackend_) {
        $httpBackend = _$httpBackend_;
    }));

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe("Viaje domain", function() {

        var Viaje;

        beforeEach(angular.mock.inject(function(_Viaje_) {
            Viaje = _Viaje_;
        }));

        it("should be tested", function() {
            expect(true).toEqual(true);
        });

    });

});
